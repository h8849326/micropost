class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :update_login, :is_login?
  include SessionsHelper
  include MpsHelper
  def update_login
    session[:user_id] = nil   if current_user && current_user.online == false 
  end

  def is_login?
    if !logged_in? 
      if  params[:unlogin] == nil
        redirect_to "http://sso.tiny.com/login?url=#{store_location}&login=1"
      end        
    end
  end

  def url_root(url)
  	url.split("/")[0] + "//" + url.split("/")[2] + "/"
  end

  private

    # 确保用户已登录
    
 


  
end
