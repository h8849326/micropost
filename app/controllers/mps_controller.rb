class MpsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user, only: :destroy
	def create
		@mp = current_user.mps.build(mp_params)
    		if @mp.save
      			flash[:success] = "Micropost created!"
      			redirect_to root_url
    		else
    			@feed_item = []
      			render 'static_pages/home'
  			end
	end

	def destroy
		@mp.destroy
		flash[:success] = "Micropost deleted!"
		redirect_to request.referrer || root_url
	end

	private
		def mp_params
			params.require(:mp).permit(:content)
		end

		def correct_user
			@mp = current_user.mps.find_by(id: params[:id])
			redirect_to root_url if @mp.nil?
		end
end
