class SessionsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:destroy]
  def new
    if params[:f] == nil
      redirect_to "http://sso.tiny.com/login?url=http://tnmp.tiny.com/login"
    else
      authen_url?(params)
    end
  end

  def create
  	@user = User.find_by(email: params[:session][:email].downcase )
  	if @user&&@user.authenticate(params[:session][:password])
      if @user.activated?
  		log_in @user
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
  		flash[:success] = "Welcome"
  		redirect_back_or @user
      else
        flash[:dager] = "Account not activated"
        redirect_to root_url
      end
  		#user.logged
  	else
  		flash.now[:danger] = "Invaild email/password combination"
        render 'new'
    end

  end

  def destroy
  	log_out if logged_in?
  end

  
end
