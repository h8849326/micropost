class StaticPagesController < ApplicationController
  def home
  	if logged_in?
    @mp  = current_user.mps.build
    @feed_items = current_user.feed.paginate(page: params[:page])
	end
  end

  def help
  end
end
