module SessionsHelper
  def log_in(user)
		session[:user_id] = user.id
    user.online = true
  end

  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def current_user
    if (user_id = session[:user_id])
  	   @current_user ||=User.find_by(id: session[:user_id])
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  def logged_in?
  	!current_user.nil?
  end

  def log_out
    forget(current_user)
  	user = current_user
    user.online = false
    user.save
    session[:user_id] = nil
    redirect_to root_url
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def current_user?(user)
    user == current_user
  end

  def store_location
     session[:forwarding_url] = "http://tnmp.tiny.com" + (request.original_url).split("?")[0].split("http://127.0.0.1:3002")[1] if request.get?
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def authen_url?(url)
    if BCrypt::Password.new(url[:f]) == "9fj90k.'f9f9$imjlnfdiso9/%#,f&#{(url[:time].to_i-95536).to_s}#{url[:user]}"
      user = User.find_by(email: url[:user])
      log_in user
      redirect_to user
    else
      redirect_to "http://sso.tiny.com/login?url=http://tnmp.tiny.com/login/"
    end
  end



	
end
