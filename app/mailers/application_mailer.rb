class ApplicationMailer < ActionMailer::Base
  default from: 'Tiny@example.com'
  layout 'mailer'
end
