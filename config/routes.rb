Rails.application.routes.draw do

  get '/login', to: 'sessions#new'

  get '/signup', to: 'users#new'

  get '/help', to: 'static_pages#help' 

  get '/about', to: 'static_pages#about'

  get '/contact', to: 'static_pages#contact'

  root 'static_pages#home'

  post '/signup' , to: 'users#create'

  post '/login', to: 'sessions#create'

  delete '/logout', to: 'sessions#destroy' 

  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :account_activations, only: [:edit]

  resources :pwd_resets,          only: [:edit, :new, :create, :update]

  resources :mps,                 only: [:create, :destroy]

  resources :relationships,       only: [:create, :destroy]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
