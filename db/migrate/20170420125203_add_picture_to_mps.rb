class AddPictureToMps < ActiveRecord::Migration[5.0]
  def change
    add_column :mps, :picture, :string
  end
end
