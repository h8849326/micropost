require 'test_helper'

class MpsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @mp = mps(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Mp.count' do
      post mps_path, params: { mp: { content: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Mp.count' do
      delete mp_path(@mp)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong micropost" do
    log_in_as(users(:Tiny))
    mp = mps(:ants)
    assert_no_difference 'Mp.count' do
      delete mp_path(mp)
    end
    assert_redirected_to root_url
  end
end
