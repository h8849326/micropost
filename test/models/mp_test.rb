require 'test_helper'

class MpTest < ActiveSupport::TestCase
  def setup
    @user = users(:Tiny)
    # 这行代码不符合常见做法
    @mp = @user.mps.build(content: "Lorem ipsum")
  end

  test "should be valid" do
    assert @mp.valid?
  end

  test "user id should be present" do
    @mp.user_id = nil
    assert_not @mp.valid?
  end

  test "content should be present" do 
  	@mp.content = "  "
  	assert_not @mp.valid?
  end

  test "content should be at most 140 characters" do
  	@mp.content = "a"*141
  	assert_not @mp.valid?
  end

  test "order should be most recent first" do 
  	assert_equal mps(:most_recent), Mp.first
  end


end
